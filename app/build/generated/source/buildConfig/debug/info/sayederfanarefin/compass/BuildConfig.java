/**
 * Automatically generated file. DO NOT MODIFY
 */
package info.sayederfanarefin.compass;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "info.sayederfanarefin.compass";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1001;
  public static final String VERSION_NAME = "1.0.0.1-beta";
}
